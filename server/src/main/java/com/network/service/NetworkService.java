package com.network.service;

import com.network.swagger.models.NetworkAddressDTO;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class NetworkService {

    private final Runtime runtime = Runtime.getRuntime();
    private String commandToExecute;

    Pattern ipPattern = Pattern.compile("(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)");
    Pattern macPattern = Pattern.compile("([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})");

    private Map<String, String> ipMacMap = new HashMap<>();
    private List<String> ipAddresses = new ArrayList<>();
    private List<String> macAddresses = new ArrayList<>();

    public List<NetworkAddressDTO> getNetworkAddresses() {

        List<NetworkAddressDTO> networkAddressDTOList = new ArrayList<>();

        for (Map.Entry<String, String> entry : ipMacMap.entrySet()) {
            NetworkAddressDTO networkAddressDTO = new NetworkAddressDTO();
            networkAddressDTO.setIp(entry.getKey());
            networkAddressDTO.setMac(entry.getValue());
            networkAddressDTOList.add(networkAddressDTO);
        }

        return networkAddressDTOList;
    }

    public boolean ipAddressPresent(String ipAddress) {
        return ipAddresses.contains(ipAddress);
    }

    public List<String> getAllIpAddresses() {
        return new ArrayList<>(ipAddresses);
    }

    public boolean macAddressPresent(String macAddress) {
        return macAddresses.contains(macAddress);
    }

    public List<String> getAllMacAddresses() {
        return new ArrayList<>(macAddresses);
    }

    @Scheduled(fixedRate = 5000, initialDelay = 2000)
    private void refreshNetworkAddresses() throws IOException {

        Map<String, String> freshIpMacMapList = new HashMap<>();
        new BufferedReader(new InputStreamReader(runtime.exec(commandToExecute).getInputStream())).lines().forEach(line -> {
            Matcher ipMatcher = ipPattern.matcher(line);
            Matcher macMatcher = macPattern.matcher(line);
            if (ipMatcher.find() && macMatcher.find()) {
                freshIpMacMapList.put(ipMatcher.group(), macMatcher.group());
            }
        });

        ipMacMap = freshIpMacMapList;
        ipAddresses = freshIpMacMapList.keySet().stream().sorted(Comparator.nullsLast(String::compareToIgnoreCase)).collect(Collectors.toList());
        macAddresses = freshIpMacMapList.values().stream().sorted(Comparator.nullsLast(String::compareToIgnoreCase)).collect(Collectors.toList());
    }

    @PostConstruct
    public void initProcess() {
        boolean isWindows = System.getProperty("os.name").toLowerCase().contains("windows");
        if (isWindows) {
            commandToExecute = String.format("cmd.exe /c %s", "arp -a");
        } else {
            commandToExecute = String.format("sh -c %s", "arp -a");
        }
    }

}
