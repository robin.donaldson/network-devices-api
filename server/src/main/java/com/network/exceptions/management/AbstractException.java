package com.network.exceptions.management;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;

@Slf4j
@Getter
@Setter
public abstract class AbstractException {

    private String exceptionName;
    private Integer code;
    private String message;

    public AbstractException(HttpStatus httpStatus, Exception exception) {
        log.debug("Exception!", exception);
        this.exceptionName = httpStatus.getReasonPhrase();
        this.code = httpStatus.value();
        this.message = getExceptionMessage(exception, httpStatus);
    }

    private String getExceptionMessage(Exception exception, HttpStatus httpStatus) {
        return ObjectUtils.isEmpty(exception.getMessage()) ? (httpStatus.getReasonPhrase() + " Exception Handler") : exception.getMessage();
    }

}
