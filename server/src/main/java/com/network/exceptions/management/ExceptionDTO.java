package com.network.exceptions.management;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Getter
@Setter
public class ExceptionDTO extends AbstractException {

    private Map<String, String> validationErrors;

    public ExceptionDTO(HttpStatus httpStatus, Exception exception) {
        super(httpStatus, exception);
        this.validationErrors = new HashMap<>();
    }

    public ExceptionDTO(HttpStatus httpStatus, Exception exception, Map<String, String> validationErrors) {
        super(httpStatus, exception);
        this.validationErrors = validationErrors;
    }

}


