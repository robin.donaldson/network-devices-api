package com.network.exceptions.management;

import com.network.exceptions.ForbiddenException;
import com.network.exceptions.NotFoundException;
import com.network.exceptions.UnavailableException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value= {NotFoundException.class})
    public ResponseEntity<ExceptionDTO> handleNotFoundException(Exception exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO(NOT_FOUND, exception);
        return new ResponseEntity<>(exceptionDTO, NOT_FOUND);
    }

    @ExceptionHandler(value = {ForbiddenException.class})
    public ResponseEntity<ExceptionDTO> handleForbiddenException(Exception exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO(FORBIDDEN, exception);
        return new ResponseEntity<>(exceptionDTO, FORBIDDEN);
    }

    @ExceptionHandler(value = {UnavailableException.class})
    public ResponseEntity<ExceptionDTO> handleUnavailableException(Exception exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO(SERVICE_UNAVAILABLE, exception);
        return new ResponseEntity<>(exceptionDTO, SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = "Illegal Argument/State Exception";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException exception) {
        Map<String, String> validationErrors = new HashMap<>();
        for (ConstraintViolation<?> violation : exception.getConstraintViolations()) {
            validationErrors.put(violation.getPropertyPath().toString(), violation.getMessage());
        }
        ExceptionDTO exceptionDTO = new ExceptionDTO(BAD_REQUEST, exception, validationErrors);
        exceptionDTO.setMessage("Constraint Violation Exception Handler");
        return new ResponseEntity<>(exceptionDTO, BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionDTO> handleException(Exception exception) {
        ExceptionDTO exceptionDTO = new ExceptionDTO(BAD_REQUEST, exception);
        exceptionDTO.setMessage("Exception Handler");
        return new ResponseEntity<>(exceptionDTO, BAD_REQUEST);
    }

}
