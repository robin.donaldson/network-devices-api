package com.network.swagger.models;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * NetworkAddressDTO
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-10-03T00:55:00.527+01:00[Europe/London]")
public class NetworkAddressDTO  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("ip")
  private String ip;

  @JsonProperty("mac")
  private String mac;

  public NetworkAddressDTO ip(String ip) {
    this.ip = ip;
    return this;
  }

  /**
   * Get ip
   * @return ip
  */
  @ApiModelProperty(value = "")


  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public NetworkAddressDTO mac(String mac) {
    this.mac = mac;
    return this;
  }

  /**
   * Get mac
   * @return mac
  */
  @ApiModelProperty(value = "")


  public String getMac() {
    return mac;
  }

  public void setMac(String mac) {
    this.mac = mac;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NetworkAddressDTO networkAddress = (NetworkAddressDTO) o;
    return Objects.equals(this.ip, networkAddress.ip) &&
        Objects.equals(this.mac, networkAddress.mac);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ip, mac);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NetworkAddressDTO {\n");
    
    sb.append("    ip: ").append(toIndentedString(ip)).append("\n");
    sb.append("    mac: ").append(toIndentedString(mac)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

