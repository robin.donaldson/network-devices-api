package com.network.controller;

import com.network.service.NetworkService;
import com.network.swagger.api.NetworkApi;
import com.network.swagger.models.NetworkAddressDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Controller
@RequestMapping("${api.path}")
public class NetworkController implements NetworkApi {

    private final NetworkService networkService;

    public NetworkController(NetworkService networkService) {
        this.networkService = networkService;
    }

    @Override
    public ResponseEntity<List<NetworkAddressDTO>> networkAddressAllGet() {
        return new ResponseEntity<>(networkService.getNetworkAddresses(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<String>> networkIpAddressAllGet() {
        return new ResponseEntity<>(networkService.getAllIpAddresses(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<String>> networkMacAddressAllGet() {
        return new ResponseEntity<>(networkService.getAllMacAddresses(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> networkIpAddressPresentGet(@NotNull @Pattern(regexp = "^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$") @Valid String ipAddress) {
        return new ResponseEntity<>(networkService.ipAddressPresent(ipAddress), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> networkMacAddressPresentGet(@NotNull @Pattern(regexp = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$") @Valid String macAddress) {
        return new ResponseEntity<>(networkService.macAddressPresent(macAddress), HttpStatus.OK);
    }

}
