# Networked Devices Microservice

Springboot server containing API's to retrieve IP/MAC addresses for all devices on servers network.

## Build

Install JDK 1.8

Clone repo from GitLab repository.

Run maven package command to compile JAR

Ensure no other process is running on port 80.

Navigate to directory containing outputted JAR, run command: java -jar network-api.jar to start application.